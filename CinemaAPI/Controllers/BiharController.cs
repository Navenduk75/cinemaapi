﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace CinemaAPI.Controllers
{

    class Food
    {
        public string Name { get; set; }
    }

    class Baby
    {
        Food StomachFood { get; set; }

        public void Eat(Food food)
        {
            StomachFood = food;            
        }

        public Food WhatisInMyStomach()
        {
            return StomachFood;
        }
    }




    public class BiharController : ApiController
    {
        // GET: api/Bihar
        public IEnumerable<string> Get()
        {

            Baby chandan = new Baby();

            Food food1 = new Food();
            food1.Name = "Mango";

            chandan.Eat(food1);
            //chandan..StomachFood = "Mango";
            chandan.WhatisInMyStomach();

            return new string[] { "value1", "value2" };
        }

        // GET: api/Bihar/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Bihar
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Bihar/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Bihar/5
        public void Delete(int id)
        {
        }
    }
}
