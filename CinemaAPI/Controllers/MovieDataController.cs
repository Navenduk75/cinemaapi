﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data.SqlClient;

namespace CinemaAPI.Controllers
{

    //    Class/Object
    //      Properties
    //      Methods

    //    Object oriented programming construct:
    //    1. Inheritance
    //    2. Encapsulation
    //    3. Abstraction
    //    4. Polymorphism

    public class MovieData
    {
        public string name { get; set; }
        public string actors { get; set; }
        public string linkParameter { get; set; }
        public string posterUrl { get; set; }
        public Int16 rating { get; set; }
    }


    //[RoutePrefix("api/movie")]
    public class MovieDataController : ApiController
    {
        // GET: api/movie/getAMovie/5
        //[HttpGet]
        //[Route("getAMovie")]
        public MovieData Get(int id)
        {
            string query = string.Format("select * from MovieData where ID = {0}", id);
            MovieData data = new MovieData();

            try
            {
                SqlConnection conn = new SqlConnection("Data Source=(localdb)\\ProjectsV13;Initial Catalog=ComputerClass;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                SqlCommand comm = new SqlCommand(query, conn);
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    data.name = reader[1].ToString();
                    data.actors = reader[2].ToString();
                    data.linkParameter = reader[3].ToString();
                    data.posterUrl = reader[4].ToString();
                    data.rating = (Int16)reader[5];
                }
                conn.Close();
            }
            catch (Exception ex)
            {

            }

            return data;
        }


        // GET: api/movie/allMovies
        //[HttpGet]
        //[Route("allMovies")]
        public List<MovieData> Get()
        {
            List<MovieData> allData = new List<MovieData>();

            try
            {
                SqlConnection conn = new SqlConnection("Data Source=(localdb)\\ProjectsV13;Initial Catalog=ComputerClass;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                SqlCommand comm = new SqlCommand("select [Name], Actors,LinkParameter,PosterUrl, Rating from MovieData", conn);
                conn.Open();
                SqlDataReader reader = comm.ExecuteReader();
                while (reader.Read())
                {
                    MovieData data = new MovieData();
                    data.name = reader[0].ToString();
                    data.actors = reader[1].ToString();
                    data.linkParameter = reader[2].ToString();
                    data.posterUrl = reader[3].ToString();
                    data.rating = (Int16)reader[4];

                    allData.Add(data);
                }
                conn.Close();
            }
            catch(Exception ex)
            {
                
            }

            return allData;
        }


        // POST: api/movie/addMovie
        //[HttpPost]
        //[Route("addMovie")]
        public bool Post([FromBody]MovieData data)
        {
            string query = String.Format("insert into MovieData([Name], Actors, LinkParameter, PosterUrl)" +
                " values('{0}','{1}','{2}','{3}')", data.name, data.actors, data.linkParameter, data.posterUrl);
            try
            {
                SqlConnection conn = new SqlConnection("Data Source=(localdb)\\ProjectsV13;Initial Catalog=ComputerClass;Integrated Security=True;Connect Timeout=30;Encrypt=False;TrustServerCertificate=True;ApplicationIntent=ReadWrite;MultiSubnetFailover=False");
                SqlCommand comm = new SqlCommand(query, conn);
                conn.Open();
                comm.ExecuteNonQuery();
                conn.Close();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

                
        // PUT: api/movie/updateMovie/8
        //[HttpPut]
        //[Route("updateMovie")]
        public bool Put(int id, [FromBody]MovieData data)
        {
            return true;
        }


        // DELETE: api/movie/deleteMovie/3
        //[HttpDelete]
        //[Route("deleteMovie")]
        public bool Delete(int id)
        {
            return true;
        }

    }
}
