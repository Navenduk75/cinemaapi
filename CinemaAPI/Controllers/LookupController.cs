﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace CinemaAPI.Controllers
{
    public class State
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    [RoutePrefix("api/lookup")]
    public class LookupController : ApiController
    {
        // GET api/lookup/states
        [HttpGet]
        [Route("states")]
        public List<State> AllStates()
        {
            State s1 = new State();
            s1.Id = 4;
            s1.Name = "Bihar";

            State s2 = new State();
            s2.Id = 31;
            s2.Name = "Uttar Pradesh";

            State s3 = new State();
            s3.Id = 7;
            s3.Name = "Delhi";

            List<State> allStates = new List<State>();
            allStates.Add(s1);
            allStates.Add(s2);
            allStates.Add(s3);

            return allStates;

        }



    }
}